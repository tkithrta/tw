import { ErrorBoundary, LocationProvider, Route, Router, lazy } from 'preact-iso';
import withTwind from "@twind/wmr";
import Home from './pages/home/index.js';
import NotFound from './pages/_404.js';
import Header from './header.js';

const About = lazy(() => import('./pages/about/index.js'));
const Twind = lazy(() => import('./pages/twind/index.js'));

export function App() {
	return (
		<LocationProvider>
			<div class="app">
				<Header />
				<ErrorBoundary>
					<Router>
						<Route path="/" component={Home} />
						<Route path="/about" component={About} />
						<Route path="/twind" component={Twind} />
						<Route default component={NotFound} />
					</Router>
				</ErrorBoundary>
			</div>
		</LocationProvider>
	);
}

const { hydrate, prerender } = withTwind(data => <App {...data} />);

hydrate(<App />);

export { prerender };
